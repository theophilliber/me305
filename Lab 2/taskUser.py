'''!@file      TaskUser.py
    @brief      User Interface for Encoder FSM. 
    @details    The task uses the USB VCP (Virtual COM Port) to cooperatively 
                take character input from the user working at a serial terminal
                such as PuTTY. Using the keyboard, the user can perform the following
                functions: zero encoder 1 (z), print position of encoder (p), 
                print delta for encoder (d), collect data for 30 seconds and print
                to PuTTY as comma separated list (g), or end data collection
                early (s). 
    @author     Theo Philliber
    @author     Ruodolf Rumbaoa
    @date       02/2/2022
'''

from time import ticks_us, ticks_add, ticks_diff
import micropython, pyb



S0_INIT = micropython.const(0)
S1_WAIT = micropython.const(1)
S2_ZERO = micropython.const(2)
S3_POS = micropython.const(3)
S4_DELTA = micropython.const(4)
S5_RECORD = micropython.const(5)
S6_CSV = micropython.const(6)


def taskUserFcn(taskName, period, zFlag, pFlag, dFlag, gFlag, sFlag, dataList, printShare, recDone): 


    '''!@brief              Generator function to run the UI task as an FSM.
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param zFlag        Boolean variable (shares class) representing if the
                            'z' key has been pressed.
        @param pFlag        Boolean variable (shares class) representing if the
                            'p' key has been pressed.
        @param dFlag        Boolean variable (shares class) representing if the
                            'd' key has been pressed.
        @param gFlag        Boolean variable (shares class) representing if the
                            'g' key has been pressed.
        @param sFlag        Boolean variable (shares class) representing if the
                            's' key has been pressed. 
        @param dataList     Shares class used for printing comma separated list
                            of collected data. 
        @param printShare   Shares class used for storing and printing data from
                            position and delta functions called by taskEncoder.py.
        @param recDone      Boolean (shares class) telling whether recording has
                            finished or not.
                            
                            
    '''
    
    # Start at State 0, initialize welcome message with instructions
    state = S0_INIT
    print('+---------------------------------------+')
    print('+       ME305 LAB 2 DEMO                +')
    print('+      BY THEO PHILLIBER AND            +')
    print('+        RUODOLF RUMBAOA                +')
    print('+---------------------------------------+')
    print('+z: Zeroes the position of encoder 1    +')
    print('+p: Prints out the position of encoder 1+')
    print('+d: Prints out delta for encoder 1      +')
    print('+g: Get data for 30 seconds and print to+')
    print('+   PuTTY as comma separated list       +')
    print('+s: End data collection permanently     +')
    print('+---------------------------------------+')
    
    # Create virtual serial port object for getting keyboard input.
    ser = pyb.USB_VCP()
    
    # Transition to State 1: Wait for input. 
    state = S1_WAIT
    
    # Enter while loop to continually check for keyboard input. Upon input from
    # user, the state will change and the corresponding flag will raise. Upon
    # the next iteration of the taskEncoder.py function, the flag will be read,
    # and the method from encoder.py will be called.
    
    # Establish start time to control rate at which code runs.
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        # Find current time
        current_time = ticks_us()
        # Check if time period has passed since last time code ran.
        if ticks_diff(next_time, current_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
            
            # State 1: Wait for input. 
            # Check for input, read it (if existing), and raise the correct flag.
            if state == S1_WAIT:
                
                # Check for keyboard input.
                if ser.any():
                    # Read (1 byte) of keyboard input. Raise whatever flag 
                    # corresponds to it.
                    charin = ser.read(1).decode() 
                    print(charin)
                    
                    #Read character, raise corresponding flag and change state.
                    if charin == 'z':
                        zFlag.write(True)
                        state = S2_ZERO
                    elif charin == 'p':
                        pFlag.write(True)
                        state = S3_POS
                    elif charin == 'd':
                       dFlag.write(True)
                       state = S4_DELTA
                    elif charin == 'g': 
                       print('Recording data for 30 seconds. Press the s key to stop recording early. Output will be in a .CSV file')
                       gFlag.write(True)
                       state = S5_RECORD
                    elif charin == 's':
                       print('Encoder must be collecting data to stop recording! Press the g key to collect data, and s to stop it.')
                    else:
                        print('Type one of the keys as listed above')
                       
            
            elif state == S2_ZERO:
                
                if not zFlag.read():
                    print('Encoder zeroed (taskUser)')
                    #zFlag.write(False)
                    state = S1_WAIT
            
            elif state == S3_POS:
                
                if not pFlag.read():
                    print(f'Encoder Position: {printShare.read()}')
                    state = S1_WAIT
                
            elif state == S4_DELTA:
                
                if not dFlag.read():
                    print(f'Encoder Delta: {printShare.read()}')    
                    state = S1_WAIT
            
            elif state == S5_RECORD:
                
                
                
                # Check for keyboard input.
                if ser.any():
                    charin = ser.read(1).decode()            
                    if charin == 's':
                        sFlag.write(True)
                        
                
                if recDone.read():
                    state = S6_CSV
                    printNum = 0
                    
                    
            elif state == S6_CSV:
                
                if printNum < (len(dataList) - 1):
                    print(dataList[printNum])
                    
                else: 
                    recDone.write(True)
                    print('Recording Finished!')
                    print(dataList.read())
                    state = S1_WAIT
                    
            
            yield state
        else:
            
            yield None
            
                
                
            