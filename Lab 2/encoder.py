'''!@file       encoder.py
    @brief      A driver for reading from Quadrature Encoders
    @details    Driver class for incremental motor encoder. The class initialization
                parameters include the two output pins (pin1, pin2) and the
                timer channel number. The class methods will update the encoder's
                position, print the position, print the change in position, and
                set the position to zero.         
@author Theo Philliber
@author Ruodolf Rumbaoa
@date February 3, 2022
'''
import pyb





class Encoder:
    '''!@brief Interface with quadrature encoders
        @details
    '''
        
    def __init__(self, pin1, pin2, timInt):
        '''!@brief Constructs an encoder object
            @details
        '''
        print('Creating encoder object')
        ## @var pinA5
        # Pin A5 representing timer pin on board
        #pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
        #pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
       
        ## @var tim2
        # Timer object, frequency = 1000 Hz
        self.tim = pyb.Timer(timInt, prescaler=0, period=0xFFFF)
        
        ## @var t2ch1
        # Channel 1 on Timer 2 (tim2), used for PWM timing
        timch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin=pin1)
        timch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin=pin2)
        
        self.position = 0
        self.currentPosition = 0
        self.lastPosition = 0
        self.delta = 0
        
    def update(self):
        '''!@brief Updates encoder position and delta
        @details
        '''
        #('Reading encoder count and updating position and delta values')
        
        # Update t-1 and t values of position, and find the delta between the two
        self.lastPosition = self.currentPosition
        self.currentPosition = self.tim.counter()
        self.delta = self.currentPosition - self.lastPosition
       
        #Check if delta is greater than |AR|/2, meaning the encoder overflowed.
        # If overflow, add the AR value back to get corrected delta
        if self.delta > 32768:
            self.delta -= 0xFFFF
        elif self.delta < -32768:
            self.delta += 0xFFFF
        
        # Add (corrected) Delta to position to update
        self.position += self.delta
        
        
        
        
       
           
        
    def get_position(self):
        '''!@brief Returns encoder position.
            @details This code returns the position of the encoder, an integer. 
            It is adjusted to output the absolute position of the encoder, correcting
            for any overflow/autoreloading experienced by the clock counter.
            @return The position of the encoder shaft
        '''
        return self.position
            
        
    
    
    def zero(self, position):
        '''!@brief Resets the encoder position to zero
        @details Resets position to zero, as well as any other attributes used
        in the position calculation. This ensures the encoder will fully reset 
        to zero, rather than calculating the delta between the new zero position
        and the last stored position.
        '''
        
        # Zero all position-related attributes
        self.lastPosition           = 0
        self.currentPosition        = 0
        self.delta                  = 0
        self.position               = 0
        self.tim.counter(0)

        
        
        
    def get_delta(self):
        '''!@brief Returns encoder delta
        @details
        @return The change in position of the encoder shaft
        between the two most recent updates
        '''
        return self.delta
