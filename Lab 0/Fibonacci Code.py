''' @file Philliber_Fibonacci.py
1/6/2022  1:49 PM
Theo Philliber
ME 305, Lab Section 03
'''

def fib(idx):
    #Seed start of Fibonacci sequence to set pattern    
    sequence = [0, 1]
    #Add onto sequence until index is reached
    for i in range(0, idx + 1):
        sequence.append(sequence[-1] + sequence[-2])
    return(sequence[idx])
      
    
# User Interface/main loop
def fib_UI():

    #Create variable to run/quit
    run = True
    #Run until user decides to quit
    while run == True:
       
        
        # Prompt user for index
        idx_str = input('Enter an index: ')
        if idx_str.isdigit() == True:
            idx = int(idx_str)
            if idx < 0:
                print('Please enter an index greater than or equal to zero')
        else:
            print('Please enter a positive integer')
            print('')
            continue
               
    
        # Calculate and print Fibonacci value with Fib function
        print(fib(idx))
    
    
    
        # Reprompt User to continue or quit
        cont= input('Enter \'q\' to quit or press \'Enter\' to repeat with a new index: ')
        if cont == 'q':
            break
        elif cont == '':
            pass
        

            
#Run main loop/User Interface when program is called directly
if __name__ == '__main__':
    fib_UI()
    